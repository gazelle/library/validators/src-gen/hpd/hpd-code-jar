package net.ihe.gazelle.hpd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

@XmlRegistry
public class ObjectFactory {

	private final static QName _AttributeDescriptionsAttribute_QNAME = new QName("urn:oasis:names:tc:DSML:2:0:core", "attribute");
	private final static QName _BatchRequestSearchRequest_QNAME = new QName("urn:oasis:names:tc:DSML:2:0:core", "searchRequest");
	private final static QName _BatchRequestModifyRequest_QNAME = new QName("urn:oasis:names:tc:DSML:2:0:core", "modifyRequest");
	private final static QName _BatchRequestAddRequest_QNAME = new QName("urn:oasis:names:tc:DSML:2:0:core", "addRequest");
	private final static QName _BatchRequestDelRequest_QNAME = new QName("urn:oasis:names:tc:DSML:2:0:core", "delRequest");
	private final static QName _BatchRequestModDNRequest_QNAME = new QName("urn:oasis:names:tc:DSML:2:0:core", "modDNRequest");
	private final static QName _BatchRequestCompareRequest_QNAME = new QName("urn:oasis:names:tc:DSML:2:0:core", "compareRequest");
	private final static QName _BatchRequestAbandonRequest_QNAME = new QName("urn:oasis:names:tc:DSML:2:0:core", "abandonRequest");
	private final static QName _BatchRequestExtendedRequest_QNAME = new QName("urn:oasis:names:tc:DSML:2:0:core", "extendedRequest");
	private final static QName _FilterSetAnd_QNAME = new QName("urn:oasis:names:tc:DSML:2:0:core", "and");
	private final static QName _FilterSetOr_QNAME = new QName("urn:oasis:names:tc:DSML:2:0:core", "or");
	private final static QName _FilterSetNot_QNAME = new QName("urn:oasis:names:tc:DSML:2:0:core", "not");
	private final static QName _FilterSetEqualityMatch_QNAME = new QName("urn:oasis:names:tc:DSML:2:0:core", "equalityMatch");
	private final static QName _FilterSetSubstrings_QNAME = new QName("urn:oasis:names:tc:DSML:2:0:core", "substrings");
	private final static QName _FilterSetGreaterOrEqual_QNAME = new QName("urn:oasis:names:tc:DSML:2:0:core", "greaterOrEqual");
	private final static QName _FilterSetLessOrEqual_QNAME = new QName("urn:oasis:names:tc:DSML:2:0:core", "lessOrEqual");
	private final static QName _FilterSetPresent_QNAME = new QName("urn:oasis:names:tc:DSML:2:0:core", "present");
	private final static QName _FilterSetApproxMatch_QNAME = new QName("urn:oasis:names:tc:DSML:2:0:core", "approxMatch");
	private final static QName _FilterSetExtensibleMatch_QNAME = new QName("urn:oasis:names:tc:DSML:2:0:core", "extensibleMatch");
	private final static QName _BatchResponseSearchResponse_QNAME = new QName("urn:oasis:names:tc:DSML:2:0:core", "searchResponse");
	private final static QName _BatchResponseAuthResponse_QNAME = new QName("urn:oasis:names:tc:DSML:2:0:core", "authResponse");
	private final static QName _BatchResponseModifyResponse_QNAME = new QName("urn:oasis:names:tc:DSML:2:0:core", "modifyResponse");
	private final static QName _BatchResponseAddResponse_QNAME = new QName("urn:oasis:names:tc:DSML:2:0:core", "addResponse");
	private final static QName _BatchResponseDelResponse_QNAME = new QName("urn:oasis:names:tc:DSML:2:0:core", "delResponse");
	private final static QName _BatchResponseModDNResponse_QNAME = new QName("urn:oasis:names:tc:DSML:2:0:core", "modDNResponse");
	private final static QName _BatchResponseCompareResponse_QNAME = new QName("urn:oasis:names:tc:DSML:2:0:core", "compareResponse");
	private final static QName _BatchResponseExtendedResponse_QNAME = new QName("urn:oasis:names:tc:DSML:2:0:core", "extendedResponse");
	private final static QName _BatchResponseErrorResponse_QNAME = new QName("urn:oasis:names:tc:DSML:2:0:core", "errorResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public ObjectFactory() {}
    
    /**
     * Create an instance of {@link  AbandonRequest}
     * 
     */
    public AbandonRequest createAbandonRequest() {
        return new AbandonRequest();
    }

    /**
     * Create an instance of {@link  DsmlMessage}
     * 
     */
    public DsmlMessage createDsmlMessage() {
        return new DsmlMessage();
    }

    /**
     * Create an instance of {@link  Control}
     * 
     */
    public Control createControl() {
        return new Control();
    }

    /**
     * Create an instance of {@link  AddRequest}
     * 
     */
    public AddRequest createAddRequest() {
        return new AddRequest();
    }

    /**
     * Create an instance of {@link  DsmlAttr}
     * 
     */
    public DsmlAttr createDsmlAttr() {
        return new DsmlAttr();
    }

    /**
     * Create an instance of {@link  AttributeDescription}
     * 
     */
    public AttributeDescription createAttributeDescription() {
        return new AttributeDescription();
    }

    /**
     * Create an instance of {@link  AttributeDescriptions}
     * 
     */
    public AttributeDescriptions createAttributeDescriptions() {
        return new AttributeDescriptions();
    }

    /**
     * Create an instance of {@link  AttributeValueAssertion}
     * 
     */
    public AttributeValueAssertion createAttributeValueAssertion() {
        return new AttributeValueAssertion();
    }

    /**
     * Create an instance of {@link  AuthRequest}
     * 
     */
    public AuthRequest createAuthRequest() {
        return new AuthRequest();
    }

    /**
     * Create an instance of {@link  BatchRequest}
     * 
     */
    public BatchRequest createBatchRequest() {
        return new BatchRequest();
    }

    /**
     * Create an instance of {@link  SearchRequest}
     * 
     */
    public SearchRequest createSearchRequest() {
        return new SearchRequest();
    }

    /**
     * Create an instance of {@link  Filter}
     * 
     */
    public Filter createFilter() {
        return new Filter();
    }

    /**
     * Create an instance of {@link  FilterSet}
     * 
     */
    public FilterSet createFilterSet() {
        return new FilterSet();
    }

    /**
     * Create an instance of {@link  SubstringFilter}
     * 
     */
    public SubstringFilter createSubstringFilter() {
        return new SubstringFilter();
    }

    /**
     * Create an instance of {@link  MatchingRuleAssertion}
     * 
     */
    public MatchingRuleAssertion createMatchingRuleAssertion() {
        return new MatchingRuleAssertion();
    }

    /**
     * Create an instance of {@link  ModifyRequest}
     * 
     */
    public ModifyRequest createModifyRequest() {
        return new ModifyRequest();
    }

    /**
     * Create an instance of {@link  DsmlModification}
     * 
     */
    public DsmlModification createDsmlModification() {
        return new DsmlModification();
    }

    /**
     * Create an instance of {@link  DelRequest}
     * 
     */
    public DelRequest createDelRequest() {
        return new DelRequest();
    }

    /**
     * Create an instance of {@link  ModifyDNRequest}
     * 
     */
    public ModifyDNRequest createModifyDNRequest() {
        return new ModifyDNRequest();
    }

    /**
     * Create an instance of {@link  CompareRequest}
     * 
     */
    public CompareRequest createCompareRequest() {
        return new CompareRequest();
    }

    /**
     * Create an instance of {@link  ExtendedRequest}
     * 
     */
    public ExtendedRequest createExtendedRequest() {
        return new ExtendedRequest();
    }

    /**
     * Create an instance of {@link  BatchResponse}
     * 
     */
    public BatchResponse createBatchResponse() {
        return new BatchResponse();
    }

    /**
     * Create an instance of {@link  SearchResponse}
     * 
     */
    public SearchResponse createSearchResponse() {
        return new SearchResponse();
    }

    /**
     * Create an instance of {@link  SearchResultEntry}
     * 
     */
    public SearchResultEntry createSearchResultEntry() {
        return new SearchResultEntry();
    }

    /**
     * Create an instance of {@link  SearchResultReference}
     * 
     */
    public SearchResultReference createSearchResultReference() {
        return new SearchResultReference();
    }

    /**
     * Create an instance of {@link  LDAPResult}
     * 
     */
    public LDAPResult createLDAPResult() {
        return new LDAPResult();
    }

    /**
     * Create an instance of {@link  ResultCode}
     * 
     */
    public ResultCode createResultCode() {
        return new ResultCode();
    }

    /**
     * Create an instance of {@link  ExtendedResponse}
     * 
     */
    public ExtendedResponse createExtendedResponse() {
        return new ExtendedResponse();
    }

    /**
     * Create an instance of {@link  ErrorResponse}
     * 
     */
    public ErrorResponse createErrorResponse() {
        return new ErrorResponse();
    }

    /**
     * Create an instance of {@link  DetailType}
     * 
     */
    public DetailType createDetailType() {
        return new DetailType();
    }



	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AttributeDescription }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oasis:names:tc:DSML:2:0:core", name = "attribute", scope = AttributeDescriptions.class)
    public JAXBElement<net.ihe.gazelle.hpd.AttributeDescription> createAttributeDescriptionsAttribute(net.ihe.gazelle.hpd.AttributeDescription value) {
        return new JAXBElement<net.ihe.gazelle.hpd.AttributeDescription>(_AttributeDescriptionsAttribute_QNAME, net.ihe.gazelle.hpd.AttributeDescription.class, AttributeDescriptions.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oasis:names:tc:DSML:2:0:core", name = "searchRequest", scope = BatchRequest.class)
    public JAXBElement<net.ihe.gazelle.hpd.SearchRequest> createBatchRequestSearchRequest(net.ihe.gazelle.hpd.SearchRequest value) {
        return new JAXBElement<net.ihe.gazelle.hpd.SearchRequest>(_BatchRequestSearchRequest_QNAME, net.ihe.gazelle.hpd.SearchRequest.class, BatchRequest.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oasis:names:tc:DSML:2:0:core", name = "modifyRequest", scope = BatchRequest.class)
    public JAXBElement<net.ihe.gazelle.hpd.ModifyRequest> createBatchRequestModifyRequest(net.ihe.gazelle.hpd.ModifyRequest value) {
        return new JAXBElement<net.ihe.gazelle.hpd.ModifyRequest>(_BatchRequestModifyRequest_QNAME, net.ihe.gazelle.hpd.ModifyRequest.class, BatchRequest.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oasis:names:tc:DSML:2:0:core", name = "addRequest", scope = BatchRequest.class)
    public JAXBElement<net.ihe.gazelle.hpd.AddRequest> createBatchRequestAddRequest(net.ihe.gazelle.hpd.AddRequest value) {
        return new JAXBElement<net.ihe.gazelle.hpd.AddRequest>(_BatchRequestAddRequest_QNAME, net.ihe.gazelle.hpd.AddRequest.class, BatchRequest.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link DelRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oasis:names:tc:DSML:2:0:core", name = "delRequest", scope = BatchRequest.class)
    public JAXBElement<net.ihe.gazelle.hpd.DelRequest> createBatchRequestDelRequest(net.ihe.gazelle.hpd.DelRequest value) {
        return new JAXBElement<net.ihe.gazelle.hpd.DelRequest>(_BatchRequestDelRequest_QNAME, net.ihe.gazelle.hpd.DelRequest.class, BatchRequest.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyDNRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oasis:names:tc:DSML:2:0:core", name = "modDNRequest", scope = BatchRequest.class)
    public JAXBElement<net.ihe.gazelle.hpd.ModifyDNRequest> createBatchRequestModDNRequest(net.ihe.gazelle.hpd.ModifyDNRequest value) {
        return new JAXBElement<net.ihe.gazelle.hpd.ModifyDNRequest>(_BatchRequestModDNRequest_QNAME, net.ihe.gazelle.hpd.ModifyDNRequest.class, BatchRequest.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link CompareRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oasis:names:tc:DSML:2:0:core", name = "compareRequest", scope = BatchRequest.class)
    public JAXBElement<net.ihe.gazelle.hpd.CompareRequest> createBatchRequestCompareRequest(net.ihe.gazelle.hpd.CompareRequest value) {
        return new JAXBElement<net.ihe.gazelle.hpd.CompareRequest>(_BatchRequestCompareRequest_QNAME, net.ihe.gazelle.hpd.CompareRequest.class, BatchRequest.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AbandonRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oasis:names:tc:DSML:2:0:core", name = "abandonRequest", scope = BatchRequest.class)
    public JAXBElement<net.ihe.gazelle.hpd.AbandonRequest> createBatchRequestAbandonRequest(net.ihe.gazelle.hpd.AbandonRequest value) {
        return new JAXBElement<net.ihe.gazelle.hpd.AbandonRequest>(_BatchRequestAbandonRequest_QNAME, net.ihe.gazelle.hpd.AbandonRequest.class, BatchRequest.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExtendedRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oasis:names:tc:DSML:2:0:core", name = "extendedRequest", scope = BatchRequest.class)
    public JAXBElement<net.ihe.gazelle.hpd.ExtendedRequest> createBatchRequestExtendedRequest(net.ihe.gazelle.hpd.ExtendedRequest value) {
        return new JAXBElement<net.ihe.gazelle.hpd.ExtendedRequest>(_BatchRequestExtendedRequest_QNAME, net.ihe.gazelle.hpd.ExtendedRequest.class, BatchRequest.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link FilterSet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oasis:names:tc:DSML:2:0:core", name = "and", scope = FilterSet.class)
    public JAXBElement<net.ihe.gazelle.hpd.FilterSet> createFilterSetAnd(net.ihe.gazelle.hpd.FilterSet value) {
        return new JAXBElement<net.ihe.gazelle.hpd.FilterSet>(_FilterSetAnd_QNAME, net.ihe.gazelle.hpd.FilterSet.class, FilterSet.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link FilterSet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oasis:names:tc:DSML:2:0:core", name = "or", scope = FilterSet.class)
    public JAXBElement<net.ihe.gazelle.hpd.FilterSet> createFilterSetOr(net.ihe.gazelle.hpd.FilterSet value) {
        return new JAXBElement<net.ihe.gazelle.hpd.FilterSet>(_FilterSetOr_QNAME, net.ihe.gazelle.hpd.FilterSet.class, FilterSet.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link Filter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oasis:names:tc:DSML:2:0:core", name = "not", scope = FilterSet.class)
    public JAXBElement<net.ihe.gazelle.hpd.Filter> createFilterSetNot(net.ihe.gazelle.hpd.Filter value) {
        return new JAXBElement<net.ihe.gazelle.hpd.Filter>(_FilterSetNot_QNAME, net.ihe.gazelle.hpd.Filter.class, FilterSet.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AttributeValueAssertion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oasis:names:tc:DSML:2:0:core", name = "equalityMatch", scope = FilterSet.class)
    public JAXBElement<net.ihe.gazelle.hpd.AttributeValueAssertion> createFilterSetEqualityMatch(net.ihe.gazelle.hpd.AttributeValueAssertion value) {
        return new JAXBElement<net.ihe.gazelle.hpd.AttributeValueAssertion>(_FilterSetEqualityMatch_QNAME, net.ihe.gazelle.hpd.AttributeValueAssertion.class, FilterSet.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubstringFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oasis:names:tc:DSML:2:0:core", name = "substrings", scope = FilterSet.class)
    public JAXBElement<net.ihe.gazelle.hpd.SubstringFilter> createFilterSetSubstrings(net.ihe.gazelle.hpd.SubstringFilter value) {
        return new JAXBElement<net.ihe.gazelle.hpd.SubstringFilter>(_FilterSetSubstrings_QNAME, net.ihe.gazelle.hpd.SubstringFilter.class, FilterSet.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AttributeValueAssertion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oasis:names:tc:DSML:2:0:core", name = "greaterOrEqual", scope = FilterSet.class)
    public JAXBElement<net.ihe.gazelle.hpd.AttributeValueAssertion> createFilterSetGreaterOrEqual(net.ihe.gazelle.hpd.AttributeValueAssertion value) {
        return new JAXBElement<net.ihe.gazelle.hpd.AttributeValueAssertion>(_FilterSetGreaterOrEqual_QNAME, net.ihe.gazelle.hpd.AttributeValueAssertion.class, FilterSet.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AttributeValueAssertion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oasis:names:tc:DSML:2:0:core", name = "lessOrEqual", scope = FilterSet.class)
    public JAXBElement<net.ihe.gazelle.hpd.AttributeValueAssertion> createFilterSetLessOrEqual(net.ihe.gazelle.hpd.AttributeValueAssertion value) {
        return new JAXBElement<net.ihe.gazelle.hpd.AttributeValueAssertion>(_FilterSetLessOrEqual_QNAME, net.ihe.gazelle.hpd.AttributeValueAssertion.class, FilterSet.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AttributeDescription }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oasis:names:tc:DSML:2:0:core", name = "present", scope = FilterSet.class)
    public JAXBElement<net.ihe.gazelle.hpd.AttributeDescription> createFilterSetPresent(net.ihe.gazelle.hpd.AttributeDescription value) {
        return new JAXBElement<net.ihe.gazelle.hpd.AttributeDescription>(_FilterSetPresent_QNAME, net.ihe.gazelle.hpd.AttributeDescription.class, FilterSet.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AttributeValueAssertion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oasis:names:tc:DSML:2:0:core", name = "approxMatch", scope = FilterSet.class)
    public JAXBElement<net.ihe.gazelle.hpd.AttributeValueAssertion> createFilterSetApproxMatch(net.ihe.gazelle.hpd.AttributeValueAssertion value) {
        return new JAXBElement<net.ihe.gazelle.hpd.AttributeValueAssertion>(_FilterSetApproxMatch_QNAME, net.ihe.gazelle.hpd.AttributeValueAssertion.class, FilterSet.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link MatchingRuleAssertion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oasis:names:tc:DSML:2:0:core", name = "extensibleMatch", scope = FilterSet.class)
    public JAXBElement<net.ihe.gazelle.hpd.MatchingRuleAssertion> createFilterSetExtensibleMatch(net.ihe.gazelle.hpd.MatchingRuleAssertion value) {
        return new JAXBElement<net.ihe.gazelle.hpd.MatchingRuleAssertion>(_FilterSetExtensibleMatch_QNAME, net.ihe.gazelle.hpd.MatchingRuleAssertion.class, FilterSet.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oasis:names:tc:DSML:2:0:core", name = "searchResponse", scope = BatchResponse.class)
    public JAXBElement<net.ihe.gazelle.hpd.SearchResponse> createBatchResponseSearchResponse(net.ihe.gazelle.hpd.SearchResponse value) {
        return new JAXBElement<net.ihe.gazelle.hpd.SearchResponse>(_BatchResponseSearchResponse_QNAME, net.ihe.gazelle.hpd.SearchResponse.class, BatchResponse.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link LDAPResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oasis:names:tc:DSML:2:0:core", name = "authResponse", scope = BatchResponse.class)
    public JAXBElement<net.ihe.gazelle.hpd.LDAPResult> createBatchResponseAuthResponse(net.ihe.gazelle.hpd.LDAPResult value) {
        return new JAXBElement<net.ihe.gazelle.hpd.LDAPResult>(_BatchResponseAuthResponse_QNAME, net.ihe.gazelle.hpd.LDAPResult.class, BatchResponse.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link LDAPResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oasis:names:tc:DSML:2:0:core", name = "modifyResponse", scope = BatchResponse.class)
    public JAXBElement<net.ihe.gazelle.hpd.LDAPResult> createBatchResponseModifyResponse(net.ihe.gazelle.hpd.LDAPResult value) {
        return new JAXBElement<net.ihe.gazelle.hpd.LDAPResult>(_BatchResponseModifyResponse_QNAME, net.ihe.gazelle.hpd.LDAPResult.class, BatchResponse.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link LDAPResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oasis:names:tc:DSML:2:0:core", name = "addResponse", scope = BatchResponse.class)
    public JAXBElement<net.ihe.gazelle.hpd.LDAPResult> createBatchResponseAddResponse(net.ihe.gazelle.hpd.LDAPResult value) {
        return new JAXBElement<net.ihe.gazelle.hpd.LDAPResult>(_BatchResponseAddResponse_QNAME, net.ihe.gazelle.hpd.LDAPResult.class, BatchResponse.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link LDAPResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oasis:names:tc:DSML:2:0:core", name = "delResponse", scope = BatchResponse.class)
    public JAXBElement<net.ihe.gazelle.hpd.LDAPResult> createBatchResponseDelResponse(net.ihe.gazelle.hpd.LDAPResult value) {
        return new JAXBElement<net.ihe.gazelle.hpd.LDAPResult>(_BatchResponseDelResponse_QNAME, net.ihe.gazelle.hpd.LDAPResult.class, BatchResponse.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link LDAPResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oasis:names:tc:DSML:2:0:core", name = "modDNResponse", scope = BatchResponse.class)
    public JAXBElement<net.ihe.gazelle.hpd.LDAPResult> createBatchResponseModDNResponse(net.ihe.gazelle.hpd.LDAPResult value) {
        return new JAXBElement<net.ihe.gazelle.hpd.LDAPResult>(_BatchResponseModDNResponse_QNAME, net.ihe.gazelle.hpd.LDAPResult.class, BatchResponse.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link LDAPResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oasis:names:tc:DSML:2:0:core", name = "compareResponse", scope = BatchResponse.class)
    public JAXBElement<net.ihe.gazelle.hpd.LDAPResult> createBatchResponseCompareResponse(net.ihe.gazelle.hpd.LDAPResult value) {
        return new JAXBElement<net.ihe.gazelle.hpd.LDAPResult>(_BatchResponseCompareResponse_QNAME, net.ihe.gazelle.hpd.LDAPResult.class, BatchResponse.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExtendedResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oasis:names:tc:DSML:2:0:core", name = "extendedResponse", scope = BatchResponse.class)
    public JAXBElement<net.ihe.gazelle.hpd.ExtendedResponse> createBatchResponseExtendedResponse(net.ihe.gazelle.hpd.ExtendedResponse value) {
        return new JAXBElement<net.ihe.gazelle.hpd.ExtendedResponse>(_BatchResponseExtendedResponse_QNAME, net.ihe.gazelle.hpd.ExtendedResponse.class, BatchResponse.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link ErrorResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oasis:names:tc:DSML:2:0:core", name = "errorResponse", scope = BatchResponse.class)
    public JAXBElement<net.ihe.gazelle.hpd.ErrorResponse> createBatchResponseErrorResponse(net.ihe.gazelle.hpd.ErrorResponse value) {
        return new JAXBElement<net.ihe.gazelle.hpd.ErrorResponse>(_BatchResponseErrorResponse_QNAME, net.ihe.gazelle.hpd.ErrorResponse.class, BatchResponse.class, value);
    }

}