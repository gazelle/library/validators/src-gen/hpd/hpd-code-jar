/**
 * SearchResponse.java
 *
 * File generated from the core::SearchResponse uml Class
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.hpd;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;

import org.w3c.dom.Document;
import org.w3c.dom.Node;


/**
 * Description of the class SearchResponse.
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchResponse", propOrder = {
	"searchResultEntry",
	"searchResultReference",
	"searchResultDone",
	"requestID"
})
@XmlRootElement(name = "SearchResponse")
public class SearchResponse implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@XmlElement(name = "searchResultEntry", namespace = "urn:oasis:names:tc:DSML:2:0:core")
	public List<net.ihe.gazelle.hpd.SearchResultEntry> searchResultEntry;
	@XmlElement(name = "searchResultReference", namespace = "urn:oasis:names:tc:DSML:2:0:core")
	public List<net.ihe.gazelle.hpd.SearchResultReference> searchResultReference;
	@XmlElement(name = "searchResultDone", required = true, namespace = "urn:oasis:names:tc:DSML:2:0:core")
	public net.ihe.gazelle.hpd.LDAPResult searchResultDone;
	@XmlAttribute(name = "requestID")
	public java.lang.String requestID;
	
	/**
	 * An attribute containing marshalled element node
	 */
	@XmlTransient
	private org.w3c.dom.Node _xmlNodePresentation;
	
	
	/**
	 * Return searchResultEntry.
	 * @return searchResultEntry
	 */
	public List<net.ihe.gazelle.hpd.SearchResultEntry> getSearchResultEntry() {
		if (searchResultEntry == null) {
	        searchResultEntry = new ArrayList<net.ihe.gazelle.hpd.SearchResultEntry>();
	    }
	    return searchResultEntry;
	}
	
	/**
	 * Set a value to attribute searchResultEntry.
	 * @param searchResultEntry.
	 */
	public void setSearchResultEntry(List<net.ihe.gazelle.hpd.SearchResultEntry> searchResultEntry) {
	    this.searchResultEntry = searchResultEntry;
	}
	
	
	
	/**
	 * Add a searchResultEntry to the searchResultEntry collection.
	 * @param searchResultEntry_elt Element to add.
	 */
	public void addSearchResultEntry(net.ihe.gazelle.hpd.SearchResultEntry searchResultEntry_elt) {
	    this.getSearchResultEntry().add(searchResultEntry_elt);
	}
	
	/**
	 * Remove a searchResultEntry to the searchResultEntry collection.
	 * @param searchResultEntry_elt Element to remove
	 */
	public void removeSearchResultEntry(net.ihe.gazelle.hpd.SearchResultEntry searchResultEntry_elt) {
	    this.getSearchResultEntry().remove(searchResultEntry_elt);
	}
	
	/**
	 * Return searchResultReference.
	 * @return searchResultReference
	 */
	public List<net.ihe.gazelle.hpd.SearchResultReference> getSearchResultReference() {
		if (searchResultReference == null) {
	        searchResultReference = new ArrayList<net.ihe.gazelle.hpd.SearchResultReference>();
	    }
	    return searchResultReference;
	}
	
	/**
	 * Set a value to attribute searchResultReference.
	 * @param searchResultReference.
	 */
	public void setSearchResultReference(List<net.ihe.gazelle.hpd.SearchResultReference> searchResultReference) {
	    this.searchResultReference = searchResultReference;
	}
	
	
	
	/**
	 * Add a searchResultReference to the searchResultReference collection.
	 * @param searchResultReference_elt Element to add.
	 */
	public void addSearchResultReference(net.ihe.gazelle.hpd.SearchResultReference searchResultReference_elt) {
	    this.getSearchResultReference().add(searchResultReference_elt);
	}
	
	/**
	 * Remove a searchResultReference to the searchResultReference collection.
	 * @param searchResultReference_elt Element to remove
	 */
	public void removeSearchResultReference(net.ihe.gazelle.hpd.SearchResultReference searchResultReference_elt) {
	    this.getSearchResultReference().remove(searchResultReference_elt);
	}
	
	/**
	 * Return searchResultDone.
	 * @return searchResultDone
	 */
	public net.ihe.gazelle.hpd.LDAPResult getSearchResultDone() {
	    return searchResultDone;
	}
	
	/**
	 * Set a value to attribute searchResultDone.
	 * @param searchResultDone.
	 */
	public void setSearchResultDone(net.ihe.gazelle.hpd.LDAPResult searchResultDone) {
	    this.searchResultDone = searchResultDone;
	}
	
	
	
	
	/**
	 * Return requestID.
	 * @return requestID
	 */
	public java.lang.String getRequestID() {
	    return requestID;
	}
	
	/**
	 * Set a value to attribute requestID.
	 * @param requestID.
	 */
	public void setRequestID(java.lang.String requestID) {
	    this.requestID = requestID;
	}
	
	
	
	
	
	public Node get_xmlNodePresentation() {
		if (_xmlNodePresentation == null){
				JAXBContext jc;
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				dbf.setNamespaceAware(true);
				DocumentBuilder db = null;
				Document doc = null;
				try {
					db = dbf.newDocumentBuilder();
					doc = db.newDocument();
				} catch (ParserConfigurationException e1) {}
				try {
					jc = JAXBContext.newInstance("net.ihe.gazelle.hpd");
					Marshaller m = jc.createMarshaller();
					m.marshal(this, doc);
					_xmlNodePresentation = doc.getElementsByTagNameNS("urn:oasis:names:tc:DSML:2:0:core", "SearchResponse").item(0);
				} catch (JAXBException e) {
					try{
						db = dbf.newDocumentBuilder();
						_xmlNodePresentation = db.newDocument();
					}
					catch(Exception ee){}
				}
			}
			return _xmlNodePresentation;
	}
	
	public void set_xmlNodePresentation(Node _xmlNodePresentation) {
		this._xmlNodePresentation = _xmlNodePresentation;
	}
	
	
	

	
	/**
     * validate by a module of validation
     * 
     */
   public static void validateByModule(SearchResponse searchResponse, String _location, ConstraintValidatorModule cvm, List<net.ihe.gazelle.validation.Notification> diagnostic){
   		if (searchResponse != null){
   			cvm.validate(searchResponse, _location, diagnostic);
			{
				int i = 0;
				for (net.ihe.gazelle.hpd.SearchResultEntry searchResultEntry: searchResponse.getSearchResultEntry()){
					net.ihe.gazelle.hpd.SearchResultEntry.validateByModule(searchResultEntry, _location + "/searchResultEntry[" + i + "]", cvm, diagnostic);
					i++;
				}
			}
			
			{
				int i = 0;
				for (net.ihe.gazelle.hpd.SearchResultReference searchResultReference: searchResponse.getSearchResultReference()){
					net.ihe.gazelle.hpd.SearchResultReference.validateByModule(searchResultReference, _location + "/searchResultReference[" + i + "]", cvm, diagnostic);
					i++;
				}
			}
			
			net.ihe.gazelle.hpd.LDAPResult.validateByModule(searchResponse.getSearchResultDone(), _location + "/searchResultDone", cvm, diagnostic);
    	}
    }

}