/**
 * SearchResultReference.java
 *
 * File generated from the core::SearchResultReference uml Class
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.hpd;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;

import org.w3c.dom.Document;
import org.w3c.dom.Node;


/**
 * Description of the class SearchResultReference.
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchResultReference", propOrder = {
	"ref"
})
@XmlRootElement(name = "SearchResultReference")
public class SearchResultReference extends net.ihe.gazelle.hpd.DsmlMessage implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@XmlElement(name = "ref", required = true, namespace = "urn:oasis:names:tc:DSML:2:0:core")
	public List<java.lang.String> ref;
	
	/**
	 * An attribute containing marshalled element node
	 */
	@XmlTransient
	private org.w3c.dom.Node _xmlNodePresentation;
	
	
	/**
	 * Return ref.
	 * @return ref
	 */
	public List<java.lang.String> getRef() {
		if (ref == null) {
	        ref = new ArrayList<java.lang.String>();
	    }
	    return ref;
	}
	
	/**
	 * Set a value to attribute ref.
	 * @param ref.
	 */
	public void setRef(List<java.lang.String> ref) {
	    this.ref = ref;
	}
	
	
	
	/**
	 * Add a ref to the ref collection.
	 * @param ref_elt Element to add.
	 */
	public void addRef(java.lang.String ref_elt) {
	    this.getRef().add(ref_elt);
	}
	
	/**
	 * Remove a ref to the ref collection.
	 * @param ref_elt Element to remove
	 */
	public void removeRef(java.lang.String ref_elt) {
	    this.getRef().remove(ref_elt);
	}
	
	
	public Node get_xmlNodePresentation() {
		if (_xmlNodePresentation == null){
				JAXBContext jc;
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				dbf.setNamespaceAware(true);
				DocumentBuilder db = null;
				Document doc = null;
				try {
					db = dbf.newDocumentBuilder();
					doc = db.newDocument();
				} catch (ParserConfigurationException e1) {}
				try {
					jc = JAXBContext.newInstance("net.ihe.gazelle.hpd");
					Marshaller m = jc.createMarshaller();
					m.marshal(this, doc);
					_xmlNodePresentation = doc.getElementsByTagNameNS("urn:oasis:names:tc:DSML:2:0:core", "SearchResultReference").item(0);
				} catch (JAXBException e) {
					try{
						db = dbf.newDocumentBuilder();
						_xmlNodePresentation = db.newDocument();
					}
					catch(Exception ee){}
				}
			}
			return _xmlNodePresentation;
	}
	
	public void set_xmlNodePresentation(Node _xmlNodePresentation) {
		this._xmlNodePresentation = _xmlNodePresentation;
	}
	
	
	

	
	/**
     * validate by a module of validation
     * 
     */
   public static void validateByModule(SearchResultReference searchResultReference, String _location, ConstraintValidatorModule cvm, List<net.ihe.gazelle.validation.Notification> diagnostic){
   		if (searchResultReference != null){
   			net.ihe.gazelle.hpd.DsmlMessage.validateByModule(searchResultReference, _location, cvm, diagnostic);
    	}
    }

}