package net.ihe.gazelle.hpd.gui;

public class DsmlAttrValue {

    private String guiValue;

    public DsmlAttrValue(String val) {
        this.guiValue = val;
    }


    public String getGuiValue() {
        return guiValue;
    }

    public void setGuiValue(String guiValue) {
        this.guiValue = guiValue;
    }

    public DsmlAttrValue(){

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DsmlAttrValue)) {
            return false;
        }

        DsmlAttrValue that = (DsmlAttrValue) o;

        return guiValue != null ? guiValue.equals(that.guiValue) : that.guiValue == null;
    }

    @Override
    public int hashCode() {
        return guiValue != null ? guiValue.hashCode() : 0;
    }
}
