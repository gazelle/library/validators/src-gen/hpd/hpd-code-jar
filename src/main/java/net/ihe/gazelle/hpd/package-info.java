@javax.xml.bind.annotation.XmlSchema(namespace = "urn:oasis:names:tc:DSML:2:0:core", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED, xmlns = {
        @XmlNs(prefix="xsd", namespaceURI="http://www.w3.org/2001/XMLSchema")
})
package net.ihe.gazelle.hpd;

import javax.xml.bind.annotation.XmlNs;
