package net.ihe.gazelle.hpdTransformer;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class HPDTransformer {
	
	@SuppressWarnings("unchecked")
	public static <T> T  unmarshallMessage(Class<T> messageType, InputStream is) throws JAXBException{
		JAXBContext jc = JAXBContext.newInstance(messageType);
		Unmarshaller u = jc.createUnmarshaller();
		T object = (T) u.unmarshal(is);
		return object;
	}
	
	public static <T> void marshallMessage(Class<T> messageType, OutputStream out, T message) throws JAXBException{
		JAXBContext jc = JAXBContext.newInstance(messageType);
		Marshaller m = jc.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		m.marshal(message, out);		
	}
	
	public static <T> byte[] getMessageAsByteArray(Class<T> messageType, T message) throws JAXBException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		marshallMessage(messageType, baos, message);
		return baos.toByteArray();
	}
}
